/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.ditaxmlutil;

import java.io.File;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.*;

/**
 *
 * @author Takashi
 */
public class DitaDTDTest {

    private String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE task PUBLIC \"-//OASIS//DTD DITA Task//EN\" \"http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/task.dtd\"><task xmlns:ditaarch=\"http://dita.oasis-open.org/architecture/2005/\" id=\"foobar\"><title>task dita title</title><shortdesc>shortdesc shortdesc</shortdesc><taskbody><context><p>Context for the current task</p></context></taskbody></task>";

    private Document document;
    
    private final Logger logger;

    public DitaDTDTest() {
        logger = (Logger) LoggerFactory.getLogger(this.getClass());
    }

    @BeforeClass
    public static void setUpClass() {
        // DitaDTD.
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setPublicDTD method, of class DitaDTD.
     */
    @Test
    public void testSetPublicDTD_File() throws Exception {
        System.out.println("setPublicDTD");
        File xmlfile = null;
        DitaDTD.setPublicDTD(xmlfile);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setPublicDTD method, of class DitaDTD.
     */
    @Test
    public void testSetPublicDTD_String() throws Exception {
        System.out.println("setPublicDTD");
        String XML = "";
        String expResult = "";
        String result = DitaDTD.setPublicDTD(XML);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setLocalDTD method, of class DitaDTD.
     */
    @Test
    public void testSetLocalDTD_File() throws Exception {
        System.out.println("setLocalDTD");
        File xmlfile = null;
        DitaDTD.setLocalDTD(xmlfile);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setLocalDTD method, of class DitaDTD.
     */
    @Test
    public void testSetLocalDTD_String() throws Exception {
        System.out.println("setLocalDTD");
        String XML = "";
        String expResult = "";
        String result = DitaDTD.setLocalDTD(XML);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setDTDFiles method, of class DitaDTD.
     */
    @Test
    public void testSetDTDFiles_String() {
        System.out.println("setDTDFiles");
        String version = "";
        String expResult = "";
        String result = DitaDTD.setDTDFiles(version);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setDTDFiles method, of class DitaDTD.
     */
    @Test
    public void testSetDTDFiles_0args() throws Exception {
        System.out.println("setDTDFiles");
        String expResult = "";
        String result = DitaDTD.setDTDFiles();
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of replaceToLocalDTD method, of class DitaDTD.
     */
    @Test
    public void testReplaceToLocalDTD() {
        System.out.println("replaceToLocalDTD");
        Document doc = null;
        String dtdDir = "";
        DitaDTD.replaceToLocalDTD(doc, dtdDir);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of replaceToPublicDTD method, of class DitaDTD.
     */
    @Test
    public void testReplaceToPublicDTD() {
        System.out.println("replaceToPublicDTD");
        Document doc = null;
        String dtdDir = "";
        String topicType = "";
        DitaDTD.replaceToPublicDTD(doc, dtdDir, topicType);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getDitaTopicTypePublicID method, of class DitaDTD.
     */
    @Test
    public void testGetDitaTopicTypePublicID() {
        System.out.println("getDitaTopicTypePublicID");
        String topicType = "";
        String expResult = "";
        String result = DitaDTD.getDitaTopicTypePublicID(topicType);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getDitaTopicTypeSystemID method, of class DitaDTD.
     */
    @Test
    public void testGetDitaTopicTypeSystemID() throws Exception {
        System.out.println("getDitaTopicTypeSystemID");
        String topicType = "";
        boolean isLocalDTD = false;
        String expResult = "";
        String result = DitaDTD.getDitaTopicTypeSystemID(topicType, isLocalDTD);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of parseTextWithoutValidate method, of class DitaDTD.
     */
    @Test
    public void testParseTextWithoutValidate() {
        System.out.println("parseTextWithoutValidate");
        Document expResult;
        try {
            logger.error("aaaaaaaaaaaaaaaaaa");
            expResult = DocumentHelper.parseText(xml);
            Document result = DitaDTD.parseTextWithoutValidate(xml);
            System.out.println(expResult.asXML());
            System.out.println(result.asXML());
            assertEquals(expResult, result);
        } catch (DocumentException ex) {
            logger.warn(ex.getMessage());
            // TODO review the generated test code and remove the default call to fail.
//            fail("The test case is a prototype.");
        }
    }

}
