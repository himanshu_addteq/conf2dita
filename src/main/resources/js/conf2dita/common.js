/* common conf2dita editor and page */
/* global Base64, AJS */

var Conf2dita = {
    /* HTML data attribute prefix */
    ditaPrefix: "ditaProperty",
    ditaPrefixRegexp: null,
    /* ajax entry points */
    ctrlUrl: {
        ditaPropertyRest: "{baseUrl}/rest/conf2dita/1.0/ditaproperty.json", /* rest */
        ditaPropertyCtrl: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditapropertycontroller", /* servlet */
        tableProperty: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditatablepropertycontroller", /* servlet @todo move to rest */
        listProperty: "{baseUrl}/plugins/servlet/conf2dita/editor/ajax/ditalistpropertycontroller"  /* servlet @todo move to rest */
    },
    /* Confluence Rest API Url Regexp */
    RestUrlRexExp: {
        label: {
            post: new RegExp("/rest/ui/([0-9\.]+)/content/[0-9]+/labels$"),
            del: new RegExp("/rest/ui/[0-9\.]+/content/[0-9]+/label/[0-9]+$")
        }
    },
    /* supported dita table types */
    tableTypes: ["Choicetable", "Simpletable", "Table"],
    // var tableTypes = ["Choicetable","LcMatchTable","Reltable","Simpletable","SubjectRelTable","Table","TopicSubjectTable"];

    /* table order parameter */
    tableOrderParameter: "conf2dita_table_order",
    /* error message */
    showMessage: function (type, place, message) {
        // error, success, warning, info
        AJS.messages[type](place, {body: message});
    },
    base64encode: function (s) {
        return Base64.encode(s).replace(/=/g, "_").replace(/\+/g, "@").replace(/\//g, "-");
    },
    base64decode: function (s) {
        return Base64.decode(s.replace(/_/g, "=").replace(/@/g, "+").replace(/\-/g, "/"));
    },
    /* split with escape expression */
    splitByChar: function () {
        var retval = [], targetstring = "", delimiter = "", limit = 0;

        if (typeof (arguments[0]) !== 'undefined')
            targetstring = arguments[0];
        if (typeof (arguments[1]) !== 'undefined')
            delimiter = arguments[1];
        if (typeof (arguments[2]) === 'number')
            limit = arguments[2];

        var chara = "", nchara = "";
        if (targetstring) {
            var escapeFlag = false, buf = "", counter = 0;

            for (var i = 0, j = targetstring.length; i < j; i++) {
                chara = targetstring.charAt(i);
                nchara = targetstring.charAt(i + 1);
                if (chara === "\\" && nchara === delimiter) {
                    escapeFlag = true;
                }
                else if (escapeFlag === false && chara === delimiter) {
                    if (buf.length > 0) {
                        retval.push(buf);
                        if (limit > 0 && ++counter >= limit) {
                            var leftovers = targetstring.substring(i + 1).replace(/\\/g, "");
                            retval.push(leftovers);
                            return retval;
                        }
                    }
                    buf = "";
                }
                else {
                    buf += chara;
                    escapeFlag = false;
                }
            }
            if (buf) {
                retval.push(buf);
            }
        }
        return retval;
    },
    /* escape for splitByChar | and = */
    escapeMacroQueryString: function (s) {
        if (typeof (s) !== "string")
            return "";
        return s.replace(/=/g, "\\=").replace(/\|/g, "\\|");
    },
    parseMacroQueryString: function (querystring) {
        var delim1 = "|", delim2 = "=";

        if (typeof (arguments[1]) !== 'undefined')
            delim1 = arguments[1];
        if (typeof (arguments[2]) !== 'undefined')
            delim2 = arguments[2];

        var params = {};
        var paramsPair = Conf2dita.splitByChar(querystring, delim1); // not data() but attr()
        for (var i = 0, j = paramsPair.length; i < j; i++) {
            var _pair = Conf2dita.splitByChar(paramsPair[i], delim2, 1);
            if (_pair.length < 2) {
                _pair.push(null);
            }
            params[_pair[0]] = _pair[1];
        }
        return params;
    },
    /* get macro parameter from macro wysiwyg table-dom */
    // split 'data-macro-parameters="base=aaa\|bbb\=ccc|conf2dita_table_order=1|frame=bottom|platform=pla\=foo\|\|\|rrr\=mmm|props=test"'
    getMacroParameter: function (macro) {
        return Conf2dita.parseMacroQueryString($(macro).attr('data-macro-parameters'));
    }
};

AJS.$((function ($) {
    AJS.log("***** common");
    /// set global parameters and functions
    var pageId;

    /* Common params for page and editor */
    Conf2dita.Common = {
        init: function () {

            Conf2dita.ditaPrefixRegexp = new RegExp("^" + Conf2dita.ditaPrefix + "(.*)$");

            var baseUrl = AJS.General.getBaseUrl();
            for (var url in Conf2dita.ctrlUrl) {
                Conf2dita.ctrlUrl[url] = Conf2dita.ctrlUrl[url].replace("{baseUrl}", baseUrl);
            }
        },
        onLoad: function () {
            pageId = AJS.params.pageId;
        },
        /* assign dita-table properties to HTML DOM Element */
        assignTableProperties: function (targetTables, onError) {
            return ; // for 5.8 release , remove table properties controller. TODO: recovery
            // load table properties
            $.ajax({
                url: Conf2dita.ctrlUrl.tableProperty,
                type: "get",
                data: {pageId: pageId}
            }).success(function (data, success, xhr) {
                if (data.property) {
                    var error = Conf2dita.Common.loadTableProperties(targetTables, data.property);
                    onError(error, data.property);
                }
            });
        },
        loadTableProperties: function (targetTables, property) {
            var error = false;
            var table, row, cell;    // property
            var tableD, rowDs, rowD, cellDs, cellD; // DOM

            var applyAttributes = function (dom, property) {
                for (var i = 0, j = property.attr.length; i < j; i++) {
                    if (property.attr[i] && property.attr[i].name && property.attr[i].value) {
                        $(dom).data(Conf2dita.ditaPrefix + property.attr[i].name, property.attr[i].value);
                    }
                }
            };


            if (property.tables && property.tables.length) {
                if (property.tables.length === targetTables.length) {
                    /* sort by order field */
                    property.tables.sort(function (a, b) {
                        return (a.order < b.order) ? -1 : 1;
                    });
                    targetTables.sort(function (a, b) {
                        var order_a = 0, order_b = 0;
                        var params_a = Conf2dita.getMacroParameter($(a).closest('table.wysiwyg-macro'));
                        if (params_a[Conf2dita.tableOrderParameter])
                            order_a = params_a[Conf2dita.tableOrderParameter];
                        var params_b = Conf2dita.getMacroParameter($(b).closest('table.wysiwyg-macro'));
                        if (params_b[Conf2dita.tableOrderParameter])
                            order_b = params_b[Conf2dita.tableOrderParameter];
                        return (order_a < order_b) ? -1 : 1;
                    });

                    for (var i = 0, j = property.tables.length; i < j; i++) {
                        table = property.tables[i];
                        tableD = targetTables.get(i);
                        // table attributes
                        applyAttributes(tableD, table);
                        // row
                        if (table.row && table.row.length > 0) {
                            rowDs = $(tableD).find('tr');
                            if (table.row.length === rowDs.length) {
                                for (var i1 = 0, j1 = table.row.length; i1 < j1; i1++) {
                                    row = table.row[i1];
                                    rowD = rowDs.get(i1);
                                    // row attributes
                                    applyAttributes(rowD, row);
                                    if (row.cell && row.cell.length > 0) {
                                        cellDs = $(rowD).find('td,th');
                                        if (row.cell.length === cellDs.length) {
                                            for (var i2 = 0, j2 = row.cell.length; i2 < j2; i2++) {
                                                applyAttributes(cellDs.get(i2), row.cell[i2]);
                                            }
                                        } else {
                                            // console.log("cell", tableD, table, row.cell.length , cellDs.length);
                                            error = true;
                                        }
                                    }
                                }
                            } else {
                                // console.log("row", table, table.row.length, rowDs.length);
                                error = true;
                            }
                        }
                    }
                } else {
                    // console.log("table", property.tables.length, targetTables.length);
                    error = true;
                }
            }
            return error;
        },
        assignListProperties: function (targetLists, onError) {
            // load table properties
            $.ajax({
                url: Conf2dita.ctrlUrl.listProperty,
                type: "get",
                data: {pageId: pageId}
            }).success(function (data, success, xhr) {
                if (data.property) {
                    var error = Conf2dita.Common.loadListProperties(targetLists, data.property);
                    onError(error, data.property);
                }
            });
        },
        loadListProperties: function (targetLists, property) {
            
        }
    };

    /* initialize */
    Conf2dita.Common.init();
    return function () {
        //var plugin_key = "jp.co.kodnet.confluence.plugins.smartDITA";
        var plugin_key = "jp.junoe.confluence.plugins.conf2dita";
        // when whole document loaded
        AJS.log("***** common onLoad");
        Conf2dita.Common.onLoad();
        AJS.I18n.get(plugin_key, function () {
        }, function () {
        });
    };
})(AJS.$));
