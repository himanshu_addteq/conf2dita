/*
 * ユーザー情報周辺に関するユーティリティクラス
 * 
 * 
 */
package jp.junoe.confluence.plugins;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import java.io.IOException;
import java.net.URI;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Takashi
 */
public class JConfluenceUser {

    private static JConfluenceUser user;

    private final UserManager userManager;

    private final LoginUriProvider loginUriProvider;

    private UserKey userKey = null;

    /**
     * Constructor
     *
     * @param userManager
     * @param loginUriProvider
     */
    public JConfluenceUser(UserManager userManager, LoginUriProvider loginUriProvider) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
    }

    /**
     * Confluenceにログインしているかどうかを返す
     *
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    public boolean isLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserProfile userProfile = this.userManager.getRemoteUser(request);
        if (userProfile != null) {
            UserKey u;
            u = userProfile.getUserKey();
            if (u != null) {
                this.userKey = u;
                return true;
            }
        }

        return false;
    }

    /**
     * 現在管理者でログインしているかどうかを返す
     *
     * @param request
     * @param response
     * @return
     * @throws java.io.IOException
     */
    public boolean isAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!this.isLogin(request, response) || this.userKey == null) {
            return false;
        } else {
            return this.userManager.isSystemAdmin(this.userKey);
        }
    }

    /**
     * ログイン画面にジャンプ
     * @param request
     * @param response
     * @throws IOException
     */
    public void redirectToLogin(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }
    
    /**
     * ログイン後に戻ってくるURIを渡す
     * @param request
     * @return 
     */
    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
