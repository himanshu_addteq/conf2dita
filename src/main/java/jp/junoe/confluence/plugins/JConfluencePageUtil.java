/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins;

import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentDataExistsException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.ConfluenceHomeGlobalConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.activation.MimetypesFileTypeMap;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Confluenceのページを扱う際に使うUtilクラス
 *
 * @author Takashi
 */
public class JConfluencePageUtil {

    /**
     *
     * @param attachmentManager
     * pattachmentgeMattachmentnattachmentger.getAttattachmentchmentMattachmentnattachmentger()等で取得できる
     * @param page 対象Pattachmentge
     * @param temp_file_path 添付ファイルのパス
     * @param fileName ファイル名
     * @param comment 添付時のコメント
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     * @throws com.atlassian.confluence.pages.AttachmentDataExistsException
     * @throws java.lang.CloneNotSupportedException
     */
    public static void attachFile(AttachmentManager attachmentManager, ContentEntityObject page, String temp_file_path, String fileName, String comment) throws FileNotFoundException, IOException, AttachmentDataExistsException, CloneNotSupportedException {
        Attachment attachment;
        String mimetype;
        File temp_file = new File(temp_file_path);
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        mimetype = mimetypesFileTypeMap.getContentType(temp_file);
        Attachment previousVersion = null;
        attachment = attachmentManager.getAttachment(page, fileName);
        if (attachment == null) {
            attachment = new Attachment(temp_file_path, mimetype, temp_file.length(), comment);
        } else {
            previousVersion = (Attachment) attachment.clone();
        }
        if (previousVersion != null) {
            if (previousVersion.getFileSize() == temp_file.length()) {
                return;
            }
        }
        attachment.setContent(page);
        attachment.setFileName(fileName);
        attachment.setFileSize(temp_file.length());
        attachment.setComment(comment);
        page.addAttachment(attachment);
        attachmentManager.saveAttachment(attachment, previousVersion, new FileInputStream(temp_file));
    }
    
    
    /**
     * Pageの祖先IDを返却 array(1, 1-1, 1-1-1,... )という感じで返却
     * @param page
     * @return 
     */
    public static String[] getAncestorIds(Page page){
        // getAncestorsは、キャッシュがひどくて信用できないので使わない
        /*
        List<Page> pages = page.getAncestors();
        String[] ids = new String[pages.size()];
        
        for (int i=0; i<pages.size(); i++){
            ids[i] = pages.get(i).getIdAsString();
        }
        */
        List<String> ids = new ArrayList<String>();
        Page parentPage = page.getParent();
        while(parentPage != null){
            ids.add(parentPage.getIdAsString());
            parentPage = parentPage.getParent();
        }
        String[] r_ids = (String[])ids.toArray(new String[0]);
        ArrayUtils.reverse(r_ids);
        return r_ids;
    }

    /**
     * 与えられたページ条件でページがあるかどうかを探す
     *
     * @param pageId ConfluenceのページID
     * @param pageTitle ページタイトル
     * @param pageManager
     * @param currentSpace 現在操作しているスペース
     *
     * @return
     */
    public static Page findTargetPage(long pageId, String pageTitle, PageManager pageManager, Space currentSpace) {
        Page target = null;
        if (pageId > 0) {
            target = pageManager.getPage(pageId);
            if (target != null) {
                if (!target.getSpaceKey().equals(currentSpace.getKey())) {
                    target = null;
                }
            }
        }
        if (target == null) {
            target = pageManager.getPage(currentSpace.getKey(), pageTitle);
        }
        return target;
    }
}
