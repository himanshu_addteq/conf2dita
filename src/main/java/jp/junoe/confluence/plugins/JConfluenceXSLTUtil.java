/*
 * XSLを適用させるメソッド
 */
package jp.junoe.confluence.plugins;

import com.atlassian.confluence.util.velocity.VelocityUtils;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.TransformerFactory;
import net.sf.saxon.Configuration;
import net.sf.saxon.FeatureKeys;
import net.sf.saxon.StandardErrorListener;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * XSLTを実行させるパーサクラス
 *
 * @author Takashi
 */
public class JConfluenceXSLTUtil {

    /**
     * XMLに対してXSLTを実行する
     *
     * @param sourceXSL XSLの中身
     * @param sourceXML 生のXML
     * @param dtd_path DTDのあるディレクトリの指定。
     * @return 変換後のXML文字列
     * @throws TransformerException XSLT時のエラー
     * @throws Exception XSLT以外のエラー
     */
    public static String parseXSLT(String sourceXSL, String sourceXML, String dtd_path) throws TransformerException, Exception {
        String xmlResult = "";

        InputStream xslStream = null, xmlStream = null;
        StreamSource xslSource = null, xmlSource = null;

        Transformer transformer = null;
        TransformerFactory tFactory = null;

        // エラーリスナーのOutputする先をバイトストリームにしてSaxonの吐き出すエラーもキャッチ
        Configuration saConfig = new net.sf.saxon.Configuration();
        StandardErrorListener se = new net.sf.saxon.StandardErrorListener();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        // Objectをセット
        se.setErrorOutput(ps);
        saConfig.setErrorListener(se);

        try {
            // xsln
            xslStream = new ByteArrayInputStream(sourceXSL.getBytes("UTF-8"));
            xslSource = new StreamSource(xslStream);
            // xml
            xmlStream = new ByteArrayInputStream(sourceXML.getBytes("UTF-8"));
            xmlSource = new StreamSource(xmlStream);

            // transformer
            // factory作成
            tFactory = new net.sf.saxon.TransformerFactoryImpl(saConfig); // errorlistener付き
            // tFactory = new net.sf.saxon.TransformerFactoryImpl();
            tFactory.setAttribute(FeatureKeys.DTD_VALIDATION, true);
            Configuration saxconf = new net.sf.saxon.Configuration();

//            tFactory = new TransformerFactoryImpl();
//            tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
//            tFactory = TransformerFactory.newInstance("org.apache.xalan.processor.TransformerFactoryImpl", null);
//            System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
//            System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.processor.TransformerFactoryImpl");
//            tFactory = TransformerFactory.newInstance();
            transformer = tFactory.newTransformer(xslSource);
            transformer.setParameter("dtd_path", dtd_path);

            // translate
            Writer outWriter = new StringWriter();
            StreamResult result = new StreamResult(outWriter);
            transformer.transform(xmlSource, result);

            xmlResult = result.getWriter().toString();

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JConfluenceXSLTUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception(ex);
        } catch (TransformerConfigurationException ex) {
            /* XSLT変換エラーの際にこにやってくる */
            String errorMsg = ex.getMessage();
            if (baos.size() > 0) {
                errorMsg  += "\n" + baos.toString();
            }
            throw new Exception(errorMsg);
        } finally {
            IOUtils.closeQuietly(xmlStream);
            IOUtils.closeQuietly(xslStream);
        }

        return xmlResult;
    }

    /**
     * 指定したXSLファイルのSourceを取得する
     *
     * @param xslPath
     * @return
     */
    public static String getXSLTSource(String xslPath, Map map) {
        String xslBody = VelocityUtils.getRenderedTemplate(xslPath, map);
        return xslBody;
    }

    /**
     * ConfluenceのDTDを実ディレクトリに設置する
     *
     * @return セットしたDTDのパス
     * @throws IOException
     */
    public static String setConfluenceDTDFiles() throws IOException {

        String temp_dir = JConfluenceFileUtil.getTemporaryDir();
        String filepath_dest = "";
        String dtdContent;

        Map<String, String> dummyMap = null;
        File dtdDestination, dtdDestinationDir;
        PrintWriter writer;

        String directoryPath = "/dtd/confluence";
        String rootDir = temp_dir + directoryPath;
        for (String filepath : confluenceDTDfiles) {
            filepath_dest = rootDir + "/" + filepath;
            dtdDestination = new File(filepath_dest);
            if (!dtdDestination.exists()) {
                dtdDestinationDir = dtdDestination.getParentFile();
                if (dtdDestinationDir.exists() || dtdDestination.getParentFile().mkdirs()) {
                    dtdContent = VelocityUtils.getRenderedTemplate(directoryPath + "/" + filepath, dummyMap);
                    writer = new PrintWriter(new BufferedWriter(new FileWriter(dtdDestination)));
                    writer.print(dtdContent);
                    IOUtils.closeQuietly(writer);
                }
            }
        }
        return rootDir + "/" + confluenceDTDfiles[0];
    }

    /**
     * DocumentからきれいなXMLを受け取る。Document.asXML()の代替
     *
     * @param document
     * @return
     */
    public static String getFormattedXML(Document document) {
        String prettyXML = "";
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("UTF-8");
        StringWriter sw = new StringWriter();
        XMLWriter writer = new XMLWriter(sw, format);
        try {
            writer.write(document);
            writer.close();
        } catch (IOException ex) {
        } finally {
            prettyXML = sw.toString();
        }
        return prettyXML;
    }

    /**
     * ConfluenceのDTDファイルリスト
     */
    private static final String[] confluenceDTDfiles = {
        "confluence.dtd",
        "xhtml-lat1.ent",
        "xhtml-special.ent",
        "xhtml-symbol.ent"
    };
}
