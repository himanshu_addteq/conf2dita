package jp.junoe.confluence.plugins.conf2dita.dita;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.conf2dita.ConfluenceSourceDocument;
import org.dom4j.Attribute;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.dom.DOMNodeHelper;
import org.slf4j.LoggerFactory;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * DitaのTableプロパティを操作する際に必要な処理<br>
 * DitaのTableプロパティは、ConfluenceがContentBodyを保存する際に余計なHTML属性値を
 * 除去するためContentBodyに保存できないので、ContentPropertyManagerで保存する
 *
 * @author Takashi
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "root")
public class TableProperty {

    /**
     * ContentPropertyManagerに保存する際のキー
     */
    public static String storeKey = "jp.junoe.confluence.plugins.conf2dita.ditatableproperties";

    /**
     * supported table type elements
     */
    public static List supportedTables = new ArrayList<String>() {
        {
            add("table");
            add("simpletable");
            add("choicetable");
        }
    };

    /**
     * return true if this system support tableName of dita
     *
     * @param tableName
     * @return
     */
    public static boolean isSupported(String tableName) {
        return TableProperty.supportedTables.contains(tableName);
    }

    @XmlElementWrapper(name = "tables")
    @XmlElement(name = "table")
    private List<Table> tables = new ArrayList<Table>();

    /**
     * Tableを加える
     *
     * @param table
     */
    public void addTable(Table table) {
        if (table != null) {
            this.tables.add(table);
        }
    }

    /**
     * 保存した際のページのヴァージョン
     */
    @XmlAttribute(name = "pageRevision")
    private int pageRevision;

    /**
     * ページの保存プロセス自体にTableプロパティを保存させる割り込み処理ができなかったので
     * Ajaxで飛ばしてTableプロパティの処理は別処理にしているが、contentpropertyの
     * 保存したページバージョンの同期に使うフラグ。これは、ページの履歴からのRevert時に 同期を行わないようにするためのものである。
     */
    @XmlAttribute(name = "pageRevisionPreservedFlag")
    private boolean pageRevisionPreservedFlag;

    public void setPageRevision(int pageRevision) {
        this.pageRevision = pageRevision;
    }

    public void setPageRevisionPreserved(boolean pageRevisionPreservedFlag) {
        this.pageRevisionPreservedFlag = pageRevisionPreservedFlag;
    }

    public boolean isPageRevisionPreservedFlag() {
        return pageRevisionPreservedFlag;
    }

    public TableProperty() {
    }

    /**
     * ConfluenceSourceDocumentからTablePropertyをExtractしてnewする
     *
     * @param confDoc
     */
    public TableProperty(ConfluenceSourceDocument confDoc) {
        extractTableProperty(confDoc);
    }

    /**
     * ConfluenceXMLからTablePropertyを抜き出して格納する (dita からConfluenceの変換の
     * 際に使う。ditaXMLから出来立てのConfluenceXMLにはprefix付の形でtablePropertyが含まれている)
     *
     * @return
     */
    private void extractTableProperty(ConfluenceSourceDocument confDoc) {
        List docTables = confDoc.selectNodes("//html:table");
        Element docTable, docRow, docCell;
        Table table;
        Table.Row row;
        Table.Cell cell;

        for (int i = 0, j = docTables.size(); i < j; i++) {
            Object t = docTables.get(i);
            if (t instanceof Element) {
                docTable = (Element) t;
                table = new Table();
                // set table attribute
                table.setAttr(getTableAttributesFromElement(docTable));
                // set table rows
                List docRows = confDoc.selectNodes("html:tbody/html:tr|html:tr", docTable);
                for (int k = 0, l = docRows.size(); k < l; k++) {
                    // Table.Row row ;
                    row = new Table.Row();
                    Object docRowOjb = docRows.get(k);
                    if (docRowOjb instanceof Element) {
                        docRow = (Element) docRowOjb;
                        row.setAttr(getTableAttributesFromElement(docRow));
                        // set table cells
                        List docCells = confDoc.selectNodes("html:td|html:th", docRow);
                        for (int m = 0, n = docCells.size(); m < n; m++) {
                            cell = new Table.Cell();
                            Object docCellOjb = docCells.get(m);
                            if (docCellOjb instanceof Element) {
                                docCell = (Element) docCellOjb;
                                List<Table.Attr> tableAttributesFromElement = getTableAttributesFromElement(docCell);
                                cell.setAttr(tableAttributesFromElement);
                            }
                            row.addCell(cell);
                        }
                        table.addRow(row);
                    }
                }

                /**
                 * このtableタグがちゃんと囲われているかをチェックしてInsert
                 * (XMLが間違ってなければほぼ問題ないはずだけど・・・)
                 */
                Element richText = docTable.getParent();
                if (richText != null && richText instanceof Element) {
                    Element tableMacro = richText.getParent();
                    if (tableMacro != null && tableMacro instanceof Element) {
                        Attribute attribute = tableMacro.attribute("name");
                        if (attribute != null) {
                            String tableName = attribute.getValue();
                            if (tableName.startsWith(Conf2dita.DITA + "-")) {
                                tableName = tableName.substring(Conf2dita.DITA.length() + 1);
                                if (isSupported(tableName)) {
                                    // set order to tableProperty
                                    int tableOrder = i + 1;
                                    table.setOrder(tableOrder);
                                    this.addTable(table);
                                    // set order to dita-table macro
                                    Element parameter = DocumentHelper.createElement("ac:parameter");
                                    parameter.addAttribute("ac:name", Table.ORDER_PARAM);
                                    parameter.addText(String.valueOf(tableOrder));
                                    List elements = tableMacro.elements();
                                    elements.add(elements.indexOf(richText), parameter);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Document.Elementの要素からAttributeを抜き出して返す
     *
     * @param element
     * @return
     */
    private List<Table.Attr> getTableAttributesFromElement(Element element) {
        List<Table.Attr> attrs = new ArrayList<Table.Attr>();
        List attributes = element.attributes();
        for (int k = 0, l = attributes.size(); k < l; k++) {
            Attribute attr;
            Object docAttr = attributes.get(k);
            if (docAttr instanceof Attribute) {
                attr = (Attribute) docAttr;
                String attrname = attr.getName();
                if (attrname.startsWith(Conf2dita.DITA_PROPERTY_PREFIX)) {
                    attrname = attrname.substring(Conf2dita.DITA_PROPERTY_PREFIX.length());
                    attrs.add(new Table.Attr(attrname, attr.getValue()));
                }
            }
        }

        return attrs;
    }

    /**
     * ConfluenceXMLにTableのプロパティを入れる(Confluence => dita の際に使う)
     *
     * @param confdoc
     */
    public void margeToConfluenceDocument(ConfluenceSourceDocument confdoc) {
        List<Element> docTables;
        confdoc.clone();

        docTables = confdoc.selectNodes("//html:table");

        Collections.sort(docTables, new ConfluenceSourceDocument.DocumentTableComparator());

        String attrName, attrValue;

        if (docTables.size() == tables.size()) {
            for (int i = 0; i < tables.size(); i++) {
                // Tables
                Table t = tables.get(i);
                Element docT = docTables.get(i);
                // append attribute
                for (Table.Attr tAttr : t.getAttr()) {
                    attrName = tAttr.getName();
                    attrValue = tAttr.getValue();
                    if (!StringUtils.isEmpty(attrName) && !StringUtils.isEmpty(attrValue)) {
                        docT.addAttribute(Conf2dita.DITA_PROPERTY_PREFIX + attrName, attrValue);
                    }
                }
                // Rows
                List<Table.Row> rows = t.getRow();
                List<Element> docRows = confdoc.selectNodes("html:tbody/html:tr|html:tr", docT);
                if (rows.size() == docRows.size()) {
                    for (int j = 0; j < rows.size(); j++) {
                        // append attribute
                        Table.Row row = rows.get(j);
                        Element docRow = docRows.get(j);
                        for (Table.Attr rAttr : row.getAttr()) {
                            attrName = rAttr.getName();
                            attrValue = rAttr.getValue();
                            if (!StringUtils.isEmpty(attrName) && !StringUtils.isEmpty(attrValue)) {
                                docRow.addAttribute(Conf2dita.DITA_PROPERTY_PREFIX + rAttr.getName(), rAttr.getValue());
                            }
                            // Cell
                            List<Table.Cell> cells = row.getCell();
                            List<Element> docCells = confdoc.selectNodes("html:td|html:th", docRow);
                            if (cells.size() == docCells.size()) {
                                for (int k = 0; k < cells.size(); k++) {
                                    Table.Cell cell = cells.get(k);
                                    Element docCell = docCells.get(k);
                                    for (Table.Attr cAttr : cell.getAttr()) {
                                        attrName = cAttr.getName();
                                        attrValue = cAttr.getValue();
                                        if (!StringUtils.isEmpty(attrName) && !StringUtils.isEmpty(attrValue)) {
                                            docCell.addAttribute(Conf2dita.DITA_PROPERTY_PREFIX + attrName, attrValue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * そのページのTableプロパティをStoreされているContentPropertyから取得
     *
     * @param content
     * @param cm
     * @return
     */
    public static TableProperty getStoredProperties(ContentEntityObject content, ContentPropertyManager cm) {
        String tablePropertyString = cm.getTextProperty(content, storeKey);
        TableProperty root = null;
        if (!StringUtils.isEmpty(tablePropertyString)) {
            root = JAXB.unmarshal(new StringReader(tablePropertyString), TableProperty.class);
        }

        return root;
    }

    public static void storeProperties(ContentEntityObject content, ContentPropertyManager contentPropertyManager, String properties) {
        TableProperty tableProperty = null;
        if (!StringUtils.isEmpty(properties)) {
            tableProperty = JAXB.unmarshal(new StringReader(properties), TableProperty.class);
            storeProperties(content, contentPropertyManager, tableProperty);
        }
    }

    public static void storeProperties(ContentEntityObject content, ContentPropertyManager contentPropertyManager, TableProperty tableProperty) {
        if (tableProperty == null) {
            contentPropertyManager.removeProperty(content, storeKey);
        } else {
            StringWriter writer = new StringWriter();
            JAXB.marshal(tableProperty, writer);
            contentPropertyManager.setTextProperty(content, storeKey, writer.toString());
        }

    }

    /**
     * contentに付随しているTablePropertyがあった場合にそのTablePropertyのバージョンをcontentのバージョンにシンクロさせる
     * 厳密的にはスレッドセーフどころか完全に別プロセスになるので、その同期性はまったく 保証されない。
     *
     * @param content
     * @param contentPropertyManager
     */
    public static void syncVersion(ContentEntityObject content, ContentPropertyManager contentPropertyManager) {
        TableProperty tProp = getStoredProperties(content, contentPropertyManager);
        if (tProp != null) {
            if (tProp.isPageRevisionPreservedFlag()) {
                tProp.setPageRevision(content.getVersion());
            }
            tProp.setPageRevisionPreserved(false);
            storeProperties(content, contentPropertyManager, tProp);
        }
    }

    /**
     * POJOとXMLとを相互変換するためのClass
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Table {

        /**
         * DitaのTable属性をXMLやHTMLにするときに属性として付与する際のPrefix. used also in JavaScript
         * editor.js and XSL dita2confImpl.xsl
         */
        public static String DITA_PREFIX = "ditaProperty";

        /**
         * Table系DITAマクロとプロパティをマッチさせるためのマクロ側の順番のパラメータ
         */
        public static String ORDER_PARAM = "conf2dita_table_order";

        /**
         * このテーブルが使われているページの中の何番目かをこのオブジェクト側に残しておく
         */
        @XmlAttribute(name = "order")
        private int order;

        @XmlElementWrapper(name = "attrs")
        @XmlElement(name = "attr")
        private List<Attr> attr;

        @XmlElementWrapper(name = "rows")
        @XmlElement(name = "row")
        private List<Row> row = new ArrayList<Table.Row>();

        public Table() {
        }

        public List<Attr> getAttr() {
            return attr;
        }

        public void setAttr(List<Attr> attr) {
            this.attr = attr;
        }

        public void addRow(Table.Row row) {
            if (row != null) {
                this.row.add(row);
            }

        }

        public List<Row> getRow() {
            return row;
        }

        public void setRow(List<Row> row) {
            this.row = row;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        /**
         * 属性値クラス このAttrのnameには TableProperty.DITA_PREFIXはつかない
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        private static class Attr {

            @XmlElement(name = "name")
            private String name;

            @XmlElement(name = "value")
            private String value;

            public Attr() {
            }

            public Attr(String name, String value) {
                this.name = name;
                this.value = value;
            }

            public String getName() {
                return name;
            }

            public String getValue() {
                return value;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setValue(String value) {
                this.value = value;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        private static class Row {

            @XmlElementWrapper(name = "attrs")
            @XmlElement(name = "attr")
            private List<Attr> attr;

            @XmlElementWrapper(name = "cells")
            @XmlElement(name = "cell")
            private List<Cell> cell = new ArrayList<Cell>();

            public Row() {
            }

            public List<Attr> getAttr() {
                return attr;
            }

            public void setAttr(List<Attr> attr) {
                this.attr = attr;
            }

            public void addCell(Table.Cell cell) {
                if (cell != null) {
                    this.cell.add(cell);
                }
            }

            public List<Cell> getCell() {
                return cell;
            }

            public void setCell(List<Cell> cell) {
                this.cell = cell;
            }

        }

        @XmlAccessorType(XmlAccessType.FIELD)
        private static class Cell {

            @XmlElementWrapper(name = "attrs")
            @XmlElement(name = "attr")
            private List<Attr> attr;

            public Cell() {
            }

            public List<Attr> getAttr() {
                return attr;
            }

            public void setAttr(List<Attr> attr) {
                this.attr = attr;
            }

        }

    }

}
