/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.condition;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.opensymphony.webwork.ServletActionContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Takashi
 */
public class SettingDitaProperty  extends BaseConfluenceCondition {

    private ContentPropertyManager contentPropertyManager;

    /**
     * Set the value of contentPropertyManager
     *
     * @param contentPropertyManager new value of contentPropertyManager
     */
    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    /**
     * DITA propertyを設定するボタンを表示するかどうかの設定
     * 新規ページ作成時、まだpageIdが設定されていない場合には非表示
     * 
     * @param context
     * @return
     */
    @Override
    protected boolean shouldDisplay(WebInterfaceContext context) {
        HttpServletRequest req = ServletActionContext.getRequest();
        String uri = req.getServletPath();
        AbstractPage page = context.getPage();

        return page != null;
    }

}
