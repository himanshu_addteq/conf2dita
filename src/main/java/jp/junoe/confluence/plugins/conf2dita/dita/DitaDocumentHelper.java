/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.dita;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

/**
 * 継承してないけど、org.dom4j.Documentに対するorg.dom4j.DocumentHelper的な
 *
 * @author Takashi
 */
public class DitaDocumentHelper {

    /**
     *
     * @return
     */
    public static TopicDocument createDocument() {
        return new TopicDocument(DocumentHelper.createDocument());
    }

    /**
     * DocumentHelper.parseText()とは違い、ValidateせずにStringからDocumentを返す
     *
     * @param xml
     * @return
     * @throws org.dom4j.DocumentException
     */
    public static Document parseTextWithoutValidate(String xml) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document doc = null;
        InputStreamReader ir = null;
        ByteArrayInputStream bais = null;
        BufferedInputStream bis = null;
        try {
            String dtdDir = DitaDTD.setDTDFiles();
            reader.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            bais = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            bis = new BufferedInputStream(bais);
            ir = new InputStreamReader(skipUTF8BOM(bis, "UTF-8"), "UTF-8");
            doc = reader.read(ir);
        } catch (IOException ex) {
            throw new DocumentException(ex.getMessage());
        } catch (SAXException ex) {
            throw new DocumentException(ex.getMessage());
        } catch (DocumentException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DocumentException(ex.getMessage());
        }
        return doc;
    }

    /**
     * UTF-8のBOMをスキップする
     */
    private static InputStream skipUTF8BOM(InputStream is, String charSet) throws Exception {

        if (!charSet.toUpperCase().equals("UTF-8")) {
            return is;
        }
        if (!is.markSupported()) {
            // マーク機能が無い場合BufferedInputStreamを被せる
            is = new BufferedInputStream(is);
        }
        is.mark(3); // 先頭にマークを付ける
        if (is.available() >= 3) {
            byte b[] = {0, 0, 0};
            is.read(b, 0, 3);
            if (b[0] != (byte) 0xEF
                    || b[1] != (byte) 0xBB
                    || b[2] != (byte) 0xBF) {
                is.reset();// BOMでない場合は先頭まで巻き戻す
            }
        }
        return is;
    }
}
