/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.servlet.editor.ajax;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jp.junoe.confluence.plugins.JConfluenceHTTPUtil;
import jp.junoe.confluence.plugins.JConfluenceUserUtil;
import jp.junoe.confluence.plugins.conf2dita.AbstractServlet;
import jp.junoe.confluence.plugins.conf2dita.dita.TableProperty;

/**
 *
 * @author Takashi
 */
public class DitaTablePropertyController extends AbstractServlet {

    private static final long serialVersionUID = 1L;

    private static final String json_tableproperty_paramname = "tableProperties";

    private final ContentPropertyManager contentPropertyManager;

    private final PageManager pageManager;

    private final PermissionManager permissionManager;

    /**
     *
     * @param pageManager
     * @param contentPropertyManager
     * @param permissionManager
     */
    public DitaTablePropertyController(PageManager pageManager, ContentPropertyManager contentPropertyManager, PermissionManager permissionManager) {
        this.contentPropertyManager = contentPropertyManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> m = new HashMap<String, Object>();
        Page page = pageManager.getPage(getPageId(req));
        Object property = null;
        TableProperty tableProperty = null;
        if (JConfluenceUserUtil.canViewPage(permissionManager, page)) {
            tableProperty = TableProperty.getStoredProperties(page, contentPropertyManager);
            m.put("property", tableProperty);
        }

        JConfluenceHTTPUtil.servletJson(resp, m);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> m = new HashMap<String, Object>();
        boolean result = false;
        String properties = req.getParameter(json_tableproperty_paramname);
        Page page = pageManager.getPage(getPageId(req));
        if (page != null) {
            TableProperty.storeProperties(page, contentPropertyManager, properties);
            result = true;
        }
        m.put("result", result);
        JConfluenceHTTPUtil.servletJson(resp, m);
    }

}
