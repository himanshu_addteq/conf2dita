package jp.junoe.confluence.plugins.conf2dita.rest;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.gzipfilter.org.tuckey.web.filters.urlrewrite.utils.NumberUtils;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluenceUserUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;

/**
 * A resource of message.
 */
@Path("/ditaproperty")
public class DitaProperty {

    private final PageManager pageManager;

    private final ContentPropertyManager contentPropertyManager;

    private final PermissionManager permissionManager;

    private final I18NBean i18n;

    /**
     *
     * @param pm
     * @param cm
     * @param perm
     */
    public DitaProperty(PageManager pm, ContentPropertyManager cm, PermissionManager perm) {
        pageManager = pm;
        contentPropertyManager = cm;
        permissionManager = perm;
        i18n = GeneralUtil.getI18n();
    }

    /**
     *
     * @param pageId
     * @return
     */
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response api(@QueryParam("pageId") String pageId) {
        Response response;

        response = checkPage(pageId);

        if (response == null) {
            
            long id = NumberUtils.stringToLong(pageId);
            Page page = pageManager.getPage(id);
            
            DitaTopicType ditaTopicType = DitaTopicType.load(page, contentPropertyManager);
            String topicId = Conf2dita.getTopicId(page, contentPropertyManager);
            
            response = Response.ok(new DitaPropertyModel(ditaTopicType, topicId)).build();
        }

        return response;
    }

    /**
     * ページをチェックする
     *
     * @param pageId
     * @return
     */
    private Response checkPage(String pageId) {
        Response response = null;

        long id = NumberUtils.stringToLong(pageId);
        Page page = pageManager.getPage(id);
        if (page == null) {
            response = RestErrorMessage.createErrorResponse(
                    Response.Status.NOT_FOUND,
                    i18n.getText("conf2dita.error.rest.page.notfound", new String[]{pageId}));
        } else {

            boolean canViewPage = JConfluenceUserUtil.canViewPage(permissionManager, page);
            if (!canViewPage) {
                response = RestErrorMessage.createErrorResponse(
                        Response.Status.UNAUTHORIZED,
                        i18n.getText("conf2dita.error.rest.page.notauthorized", new String[]{pageId}));
            }
        }
        return response;
    }

}
