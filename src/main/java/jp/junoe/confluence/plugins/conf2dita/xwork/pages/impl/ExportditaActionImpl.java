package jp.junoe.confluence.plugins.conf2dita.xwork.pages.impl;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.atlassian.core.util.PairType;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.interceptor.SessionAware;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.dsig.TransformException;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument;
import jp.junoe.confluence.plugins.conf2dita.DitaExportUtils;
import jp.junoe.confluence.plugins.conf2dita.DitaXmlUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTDValidateErrorException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Takashi
 */
abstract public class ExportditaActionImpl extends ViewPageAction implements PageAware, SessionAware {

    /**
     * このページにアサインされているDitaのTopicType
     */
    public String topicType = "";

    private BandanaManager bandanaManager;

    private AbstractPage page;

    private Map session;

    public boolean downloadable = false;

    private final String temporary_exported_file_key
            = "export.dita.zipfile.created.path";

    /**
     * Get the value of page
     *
     * @return the value of page
     */
    @Override
    public AbstractPage getPage() {
        return page;
    }

    /**
     * Set the value of page
     *
     * @param page new value of page
     */
    @Override
    public void setPage(AbstractPage page) {
        this.page = page;
    }

    /**
     * Set the value of bandanaManager
     *
     * @param bandanaManager
     */
    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    private ContentPropertyManager contentPropertyManager;

    /**
     * Set the value of contentPropertyManager
     *
     * @param contentPropertyManager new value of contentPropertyManager
     */
    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    /**
     * 子ページも内包させるかどうかオプション
     */
    private boolean includeChildren;

    /**
     * Get the value of includeChildren
     *
     * @return the value of includeChildren
     */
    public boolean getIncludeChildren() {
        return includeChildren;
    }

    /**
     * Set the value of includeChildren
     *
     * @param includeChildren new value of includeChildren
     */
    public void setIncludeChildren(boolean includeChildren) {
        this.includeChildren = includeChildren;
    }

    /**
     * 階層化オプション
     */
    private String nestOption = "";

    /**
     * Get the value of nestOption
     *
     * @return the value of nestOption
     */
    public String getNestOption() {
        return nestOption;
    }

    /**
     * Set the value of nestOption
     *
     * @param nestOption new value of nestOption
     */
    public void setNestOption(String nestOption) {
        this.nestOption = nestOption;
    }

    /**
     * Exportする際に、階層化するかしないかのオプションの選択肢
     *
     * @return the value of nestOption
     */
    public List<PairType> getNestOptions() {
        List<PairType> nestOptions = new ArrayList<PairType>();
        nestOptions.add(new PairType(getText("conf2dita.action.export.options.nesting.nest.label"), DitaExportUtils.EXPORT_NESTOPTION_NEST));
        nestOptions.add(new PairType(getText("conf2dita.action.export.options.nesting.flat.label"), DitaExportUtils.EXPORT_NESTOPTION_FLAT));
        return nestOptions;
    }

    public String getNestOptionDisplay() {
        return getText("conf2dita.action.export.options.nesting." + nestOption + ".label");
    }

    /**
     * 添付ファイルの位置決めオプション
     */
    private String attachmentPositionOption = "";

    /**
     * Get the value of attachmentPositionOption
     *
     * @return the value of attachmentPositionOption
     */
    public String getattachmentPositionOption() {
        return attachmentPositionOption;
    }

    /**
     * Set the value of attachmentPositionOption
     *
     * @param attachmentPositionOption new value of attachmentPositionOption
     */
    public void setattachmentPositionOption(String attachmentPositionOption) {
        this.attachmentPositionOption = attachmentPositionOption;
    }

    /**
     * Exportする際に、階層化するかしないかのオプションの選択肢
     *
     * @return the value of attachmentPositionOption
     */
    public List<PairType> getAttachmentPositionOptions() {
        List<PairType> attachmentPositionOptions = new ArrayList<PairType>();
        attachmentPositionOptions.add(new PairType(getText("conf2dita.action.export.options.attachment.position.separatedir.label"), "separatedir"));
        attachmentPositionOptions.add(new PairType(getText("conf2dita.action.export.options.attachment.position.withpage.label"), "withpage"));
        attachmentPositionOptions.add(new PairType(getText("conf2dita.action.export.options.attachment.position.withpagedir.label"), "withpagedir"));
        return attachmentPositionOptions;
    }

    public String getAttachmentPositionOptionDisplay() {
        return getText("conf2dita.action.export.options.attachment.position." + attachmentPositionOption + ".label");
    }

    /**
     * directory name which store attachments files
     */
    private String attachmentDir = "attachments";

    /**
     * Get the value of attachmentDir
     *
     * @return the value of attachmentDir
     */
    public String getAttachmentDir() {
        return attachmentDir;
    }

    /**
     * Set the value of attachmentDir
     *
     * @param attachmentDir new value of attachmentDir
     */
    public void setAttachmentDir(String attachmentDir) {
        this.attachmentDir = attachmentDir;
    }

    private boolean editMode = false;

    /**
     * Exportするオプションのセッティング
     *
     * @return
     */
    public String setting() {
        editMode = true;
        // set form default
        if (nestOption.equals("")) {
            nestOption = DitaExportUtils.EXPORT_NESTOPTION_FLAT;
        }
        if (attachmentPositionOption.equals("")) {
            attachmentPositionOption = "separatedir";
        }
        validateSetting(page);

        return SUCCESS;
    }

    public boolean xsltError = false;

    /**
     * check page setting
     *
     * @param page export start page
     */
    private void validateSetting(AbstractPage page) {
        ///BandanaManager
        topicType = Conf2dita.getTopicType(page, contentPropertyManager);
        TopicDocument dita = null;
        // このトピックタイプでXSLTできてDTDが通るかチェック
        if (!StringUtils.isEmpty(topicType)) {
            try {
                dita = Conf2dita.c2d(contentPropertyManager, (Page) page, topicType);
                dita.validateDTD();
                // DitaXmlUtil.validateDTD(dita);
            } catch (DitaDTDValidateErrorException ex) {
                xsltError = true;
                addFieldError("dita_dtd", ex.getDTDErrorMessage(true, true));
            } catch (TransformException ex) {
                xsltError = true;
                addFieldError("dita_dtd", getText("conf2dita.error.xslt.invalidQName"));
            } catch (Exception ex) {
                xsltError = true;
                addFieldError("dita_dtd", String.format("Error: %s at %s:%s(%d)", ex.getMessage(),
                        ex.getStackTrace()[0].getFileName(),
                        ex.getStackTrace()[0].getClassName(),
                        ex.getStackTrace()[0].getLineNumber()
                ));
            }
        } else {
            includeChildren = true;
            // DITAではないHomePageの場合はエラーを出さない
            if (page.getSpace().getHomePage().getId() != page.getId()) {
                addActionMessage(getText("conf2dita.error.dita.notopictype", new String[]{page.getTitle()}));
            }
        }

        // check attachmentsDir
        if (StringUtils.isEmpty(attachmentDir)) {
            addFieldError("attachmentDir", getText("conf2dita.action.export.error.attachmentdir.empty"));
        } else {
            if (!DitaXmlUtil.RegExp.attachmentDir.matcher(attachmentDir).find()) {
                addFieldError("attachmentDir", getText("conf2dita.action.export.error.attachmentdir.regexp"));
            }
        }
    }

    /**
     * Export実行
     *
     * @return
     */
    public String exportDo() {

        validateSetting(page);
        if (hasErrors()) {
            editMode = true;
            return INPUT;
        }

        File exportedZip;
        List<Page> pages = new ArrayList<Page>();
        Page c_page = pageManager.getPage(page.getId());
        pages.add(c_page);
        if (includeChildren) {
            pages.addAll(c_page.getDescendents());
        }
        try {
            exportedZip = createZip(pages);
        } catch (Exception ex) {
            addActionError(getText("conf2dita.action.export.error.createzip") + "[Error] " + ex.getMessage());
            return INPUT;
        }

        session.put(this.temporary_exported_file_key, exportedZip.getAbsolutePath());
        downloadable = true;
        return SUCCESS;
    }

    private InputStream exportZipInputStream;

    /**
     * Get the value of exportZipInputStream
     *
     * @return the value of exportZipInputStream
     */
    public InputStream getExportZipInputStream() {
        return exportZipInputStream;
    }

    /**
     * contentDisposition用のファイル名
     */
    private String downloadZipFileName;

    /**
     * Get the value of downloadZipFileName
     *
     * @return the value of downloadZipFileName
     */
    public String getDownloadZipFileName() {
        return downloadZipFileName;
    }

    /**
     * ファイルをダウンロードする
     *
     * @return
     */
    public String download() {
        // セッションにダウンロードするファイル名が入ってないとN.G
        File zipFile = null;
        String zipFilePath = session.get(this.temporary_exported_file_key).toString();

        if (!StringUtils.isEmpty(zipFilePath)) {
            zipFile = new File(zipFilePath);
            if (zipFile.isFile()) {
                try {
                    exportZipInputStream = FileUtils.openInputStream(zipFile);
                    // なぜかaction/reslut/param@name=contentDisposition が効かないので手動
                    HttpServletResponse response = ServletActionContext.getResponse();
                    response.reset();
                    response.setHeader("Content-Disposition", "attachment;filename=" + zipFile.getName());
                    response.setContentLength((int) zipFile.length());
                    return SUCCESS;
                } catch (IOException ex) {
                    addActionError(ex.getMessage());
                }

            }
        }

        addActionError(getText("conf2dita.action.export.error.download.nozip"));
        return INPUT;
    }

    /**
     * 編集モード
     *
     * @return
     */
    public boolean isEditMode() {
        return editMode;
    }

    /**
     *
     * @param pages
     * @return
     * @throws Exception
     */
    private File createZip(List<Page> pages) throws Exception {
        File downloadZip = null;
        Page rootPage = pageManager.getPage(page.getId());
        DitaExportUtils ditaExportUtils = new DitaExportUtils(rootPage, contentPropertyManager, pageManager, rootPage.isHomePage());
        ditaExportUtils.setNestOption(nestOption);
        ditaExportUtils.setAttachmentPositionOption(attachmentPositionOption);
        ditaExportUtils.setIncludeChildren(includeChildren);
        ditaExportUtils.setAttachmentDir(attachmentDir);
        try {
            downloadZip = ditaExportUtils.createZip(pages, "MS932");
        } catch (Exception ex) {
            throw ex;
        }
        return downloadZip;
    }

    @Override
    public boolean isPageRequired() {
        return true;
    }

    @Override
    public boolean isLatestVersionRequired() {
        return true;
    }

    @Override
    public boolean isViewPermissionRequired() {
        return true;
    }

    @Override
    public void setSession(Map map) {
        this.session = map;
    }

}
