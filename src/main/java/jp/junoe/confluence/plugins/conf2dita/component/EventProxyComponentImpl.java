package jp.junoe.confluence.plugins.conf2dita.component;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.spring.container.ContainerManager;
import jp.junoe.confluence.plugins.conf2dita.event.DitaPageSettingEvent;

public class EventProxyComponentImpl implements EventProxyComponent
{
    private final ApplicationProperties applicationProperties;
    
    private final EventPublisher eventPublisher;
    
    public EventProxyComponentImpl(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
        this.eventPublisher = (EventPublisher) ContainerManager.getInstance().getContainerContext().getComponent("eventPublisher");
    }

    @Override
    public String getName()
    {
        if(null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }
        
        return "myComponent";
    }

    /**
     * DitaのページにTopicTypeを紐づける
     * @param page
     * @param topicType
     * @param generator 何が紐づけを行ったかの目印
     */
    @Override
    public void publishDitaPageSettingEvent(ContentEntityObject page, String topicType, String generator) {
        eventPublisher.publish(new DitaPageSettingEvent(this, page, topicType, generator));
    }
}