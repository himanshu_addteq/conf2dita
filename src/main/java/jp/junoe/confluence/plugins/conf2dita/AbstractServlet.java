/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Takashi
 */
abstract public class AbstractServlet extends HttpServlet {

    /**
     * ページIDをリクエストパラメータから取得する
     *
     * @param req
     * @return
     */
    protected long getPageId(HttpServletRequest req) {
        
        String pageId = req.getParameter("pageId");
        if (pageId != null) {
            return Long.parseLong(pageId);
        }
        return 0;
    }
}
