/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jp.junoe.confluence.plugins.conf2dita.exception;

/**
 *
 * @author Takashi
 */
public abstract class Conf2ditaException extends Exception{

    public Conf2ditaException(String message) {
        super(message);
    }
    
}
