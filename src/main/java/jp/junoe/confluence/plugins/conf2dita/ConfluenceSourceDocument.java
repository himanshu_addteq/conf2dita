/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.junoe.confluence.plugins.Conf2dita;
import jp.junoe.confluence.plugins.JConfluenceHTTPUtil;
import jp.junoe.confluence.plugins.JConfluenceXSLTUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.TableProperty;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.QName;
import org.dom4j.XPath;

/**
 * ConfluenceのソースをXMLとして扱いやすくするためのラッパー
 *
 * @author Takashi
 */
public final class ConfluenceSourceDocument implements Cloneable {

    /**
     * ConfluenceのページXMLをWrapするテンプレートXMLのパス。(src/main/resourcesからの相対パス)
     */
    private static final String CONFLUENCE_XML_ROOTTAG_TEMPLATE = "/templates/xml/confluence_base.xml";

    /**
     * ConfluenceのページXMLで使用するネームスペースの一覧 ただし、Defaultにはhtmlというネームスペースを割り振っている。
     */
    private static final Map<String, String> namespace = new HashMap<String, String>() {
        {
            put("html", "http://www.atlassian.com/schema/confluence/4/");
            put("ac", "http://www.atlassian.com/schema/confluence/4/ac/");
            put("ri", "http://www.atlassian.com/schema/confluence/4/ri/");
        }
    };

    private final Document document;

    private TableProperty tableProperty;

//    private final ContentPropertyManager cm;
//
//    private final ContentEntityObject page;
    /**
     * ページオブジェクトからそのソースをXMLに納めたものを返すConstructor(Confluence=>DITAへの変換で使用する)
     *
     * @param page ページ
     * @param topicType トピックタイプ
     * @param ditaId
     * @param cm
     * @throws org.dom4j.DocumentException
     * @throws java.io.IOException
     */
    public ConfluenceSourceDocument(ContentEntityObject page, String topicType, String ditaId, ContentPropertyManager cm) throws DocumentException, IOException {

//        this.cm = cm;
//        this.page = page;
        String pageXML = page.getBodyAsString();
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put("confluenceDTDPath", JConfluenceXSLTUtil.setConfluenceDTDFiles());
        map.put("body", pageXML);
        map.put("title", page.getDisplayTitle());
        map.put("pageId", ditaId);
        map.put("topictype", topicType);
        pageXML = VelocityUtils.getRenderedTemplate(CONFLUENCE_XML_ROOTTAG_TEMPLATE, map);
        pageXML = DitaXmlUtil.EntityFilter.convertChar2NumericEntity(pageXML);
        document = DocumentHelper.parseText(pageXML);

        TableProperty storedTable = TableProperty.getStoredProperties(page, cm);
        tableProperty = storedTable;
        // image要素のditaプロパティは、ac:queryparams属性にbase64エンコードされた状態で入っているのでXSLTしやすいように変換
        List images = this.selectNodes("//ac:image");

        Element image;
        // for (Object oi: images){
        for (int i = 0, j = images.size(); i < j; i++) {
            if (images.get(i) instanceof Element) {
                image = (Element) images.get(i);
                parseImageQueryParameter(image);
            }
        }
    }

    /**
     * ConfluenceのXMLが存在する場合(DITA=>Confluenceへの変換で使用する)
     *
     * @param confxml
     * @throws org.dom4j.DocumentException
     */
    public ConfluenceSourceDocument(String confxml) throws DocumentException {
        document = DocumentHelper.parseText(confxml);
        // image要素のditaプロパティは、ac:queryparams属性にditaProperty=の形で居れる
        // ex) <ac:image ac:queryparams="ditaProperty=aWQ9YWFhfHNj...base64encoded...YXNzPXxrZXlyZWY9&amp;" ac:thumbnail="true" ac:width="300">
        parseImageElementDitaProperty();
    }

    @Override
    @SuppressWarnings("CloneDeclaresCloneNotSupported")
    public ConfluenceSourceDocument clone() {
        try {
            return (ConfluenceSourceDocument) super.clone();
        } catch (CloneNotSupportedException ex) {
            // throw new InternalError(ex.getMessage());
            return null;
        }
    }

    /**
     * tablePropertyをマージしたものを出力
     *
     * @see Document#asXML() passthru
     * @return
     */
    public String asXML() {
        ConfluenceSourceDocument clone = this.clone();
        // 出力前にtablePropertyをXMLにマージする
        if (tableProperty != null) {
            tableProperty.margeToConfluenceDocument(clone);
        }
        return clone.document.asXML();
        // return document.asXML();
    }

    /**
     * 生のXMLを出力
     *
     * @see ConfluenceSourceDocument#asXML()
     * @return
     */
    public String asRawXML() {
        return document.asXML();
    }

    /**
     * 名前空間付の場合はPrefixが必要なので別メソッドに切り出しの上、 Queryには必ずネームスペースPrefix付のものを使うこと。
     * (Default prefixは"html"。省略不可)
     *
     * @see #namespace
     * @param query
     * @return
     */
    public final List selectNodes(String query) {
        XPath xpath = document.createXPath(query);
        xpath.setNamespaceURIs(namespace);
        List nodes = xpath.selectNodes(document);
        return nodes;
    }

    /**
     * @see #selectNodes(java.lang.String)
     * @param query
     * @param node
     * @return
     */
    public List selectNodes(String query, Node node) {
        XPath xpath = node.createXPath(query);
        xpath.setNamespaceURIs(namespace);
        List nodes = xpath.selectNodes(node);
        return nodes;
    }

    private Attribute getAttribute(Element node, String prefix, String name) {
        Attribute attr = null;
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        if (!StringUtils.isEmpty(namespaceUrl)) {
            QName qname = QName.get(name, prefix, namespaceUrl);
            attr = node.attribute(qname);
        }
        return attr;
        // String queryparams = image.attributeValue(QName.get("queryparams", "ac", "http://www.atlassian.com/schema/confluence/4/ac/"));    
    }

    private void addAttribute(Element element, String prefix, String name, String value) {
        Attribute attr = null;
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        if (!StringUtils.isEmpty(namespaceUrl)) {
            QName qname = QName.get(name, prefix, namespaceUrl);
            element.addAttribute(qname, value);
        }
    }

    private void addParameter(Element element, String prefix, String name, String value) {
        String namespaceUrl = "";

        namespaceUrl = namespace.get(prefix);

        Element parameter = DocumentHelper.createElement(QName.get("parameter", prefix, namespaceUrl));
        addAttribute(parameter, prefix, "name", name);
        parameter.add(DocumentHelper.createText(value));
        element.add(parameter);
    }

    /**
     * Conflueceのimageに付けたQueryParamから属性を付ける
     *
     * @param image
     */
    private void parseImageQueryParameter(Element image) {

        Attribute imageprop;
        imageprop = getAttribute(image, "ac", "queryparams");
        if (imageprop != null) {
            String queryparams = imageprop.getValue();
            // LoggerFactory.getLogger(this.getClass()).error(queryparams);
            try {
                for (Map.Entry<String, List<String>> pquery : JConfluenceHTTPUtil.parseQuery(queryparams).entrySet()) {
                    List<String> valueList = pquery.getValue();
                    String values = "";
                    if (pquery.getKey().equals(Conf2dita.DITA_PROPERTY_PREFIX) && valueList.size() > 0) {
                        values = valueList.get(0);
                        Map<String, List<String>> decodeQueryParams = Conf2dita.Base64.decodeQueryParams(values, "|", "=");
                        for (Map.Entry<String, List<String>> pquery1 : decodeQueryParams.entrySet()) {
                            List<String> valueList1 = pquery1.getValue();
                            String key_value = null;
                            if (valueList1.size() > 0) {
                                for (String _v1 : valueList1) {
                                    if (_v1 != null && _v1.length() > 0) {
                                        key_value = _v1;
                                    }
                                    break; // only to get first ditaProperty...
                                }
                                if (key_value != null) {
                                    // image.addAttribute(new QName(
                                    addParameter(image, "ac", pquery1.getKey(), key_value);
                                }
                            }
                        }
                    }
                }
                // remove queryparams
                image.remove(imageprop);
            } catch (UnsupportedEncodingException e) {
            }
        }
    }

    /**
     * insert &gt;ac:parameter name="queryparams" value="*****"/&lt; <br>
     * value is encoded in base64 encoding
     */
    private void parseImageElementDitaProperty() {
        
        Element image;
        Element param;
        Attribute acName;
        String propPrefix = Conf2dita.DITA_PROPERTY_PREFIX + "-";

        Element queryparam;
        Map<String, String> queryparams = new HashMap<String, String>();

        for (Object _image : selectNodes("//ac:image")) {
            if (_image instanceof Element) {
                image = (Element) _image;

                for (Object _param : selectNodes("ac:parameter", image)) {
                    if (_param instanceof Element) {
                        param = (Element) _param;
                        acName = getAttribute(param, "ac","name");
                        if (acName != null && acName.getValue().startsWith(propPrefix)) {
                            queryparams.put(acName.getValue().substring(propPrefix.length()) , param.getText());
                            image.remove(param);
                        }
                    }
                }
                
                if (queryparams.size() > 0){
                    String encodeQueryParams = Conf2dita.Base64.encodeQueryParams(queryparams, "|", "=");
                    addAttribute(image, "ac", "queryparams", Conf2dita.DITA_PROPERTY_PREFIX + "=" + encodeQueryParams);
                }
            }
        }

    }

    /**
     * ConfluenceSourceDocumentの中の表を取得する際にソートするComparator
     */
    public static class DocumentTableComparator implements Comparator<Element> {

        @Override
        public int compare(Element ele1, Element ele2) {
            int order1, order2;

            order1 = getTableOrder(ele1);
            order2 = getTableOrder(ele2);

            if (order1 == order2) {
                return 0;
            } else {
                return order1 > order2 ? 1 : -1;
            }
        }

        /**
         * get order parameter of html-table wrapper dita table-macro.
         *
         * @param tableElement
         * @return
         */
        private int getTableOrder(Element tableElement) {
            int order = 0;

            Element richTextBody = tableElement.getParent();
            if (richTextBody != null) {
                Element stMacro = richTextBody.getParent();
                if (stMacro != null) {
                    String acname = stMacro.attributeValue("name");
                    if (acname.startsWith("dita-")) {
                        acname = acname.substring(5);
                    }
                    // if (TableProperty.supportedTables.contains(acname)) {
                    if (TableProperty.isSupported(acname)) {
                        Element parameter;
                        for (Object param : stMacro.selectNodes("ac:parameter")) {
                            if (param instanceof Element) {
                                parameter = (Element) param;
                                String paramName = parameter.attributeValue("name");
                                if (paramName.equals(TableProperty.Table.ORDER_PARAM)) {
                                    return (int) Long.parseLong(parameter.getText());
                                }
                            }
                        }
                    }
                }
            }

            return order;
        }
    }
}
