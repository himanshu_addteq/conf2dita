/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins.conf2dita.event;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.event.events.ConfluenceEvent;

/**
 * PageのDitaに対する外部プロパティを変更したい場合にこのEventを投げる 主に別のPluginから使われることを想定している。
 *
 * @author Takashi
 */
public class DitaPageSettingEvent extends ConfluenceEvent {

    private ContentEntityObject page;

    /**
     * Get the value of page
     *
     * @return the value of page
     */
    public ContentEntityObject getPage() {
        return page;
    }

    /**
     * Set the value of page
     *
     * @param page new value of page
     */
    public void setPage(ContentEntityObject page) {
        this.page = page;
    }

    private String topicType;

    /**
     * Get the value of topicType
     *
     * @return the value of topicType
     */
    public String getTopicType() {
        return topicType;
    }

    private String generator;

    /**
     * Get the value of generator
     *
     * @return the value of generator
     */
    public String getGenerator() {
        return generator;
    }

    /**
     * @param src event source object
     * @param page Labelの貼られたページ
     * @param topicType DITAのトピックタイプ
     * @param generator このイベントの発生元
     */
    public DitaPageSettingEvent(Object src, ContentEntityObject page, String topicType, String generator) {
        super(src);
        this.page = page;
        this.topicType = topicType;
        this.generator = generator;
    }

}
