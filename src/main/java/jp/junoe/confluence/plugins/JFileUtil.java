package jp.junoe.confluence.plugins;

import com.atlassian.gzipfilter.util.IOUtils;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Takashi
 */
public class JFileUtil {

    /**
     * UTF-8固定でファイルを読み込む(Stattachmenttic インターフェイス)
     * @deprecated FileUtils.readFileToString(file, UTF_8)を使ってください
     * @param file 読み込むファイル名
     * @return str ファイルのテキスト
     */
    public static String getFileContents(File file) {
        JFileUtil util = new JFileUtil();
        return util.getFileAsString(file);
    }
    
    
    /**
     * UTF-8固定でファイルを読み込む(実装)
     * @deprecated FileUtils.readFileToString(file, UTF_8)を使ってください
     * @param file
     * @return string ファイルのテキスト
     */
    private String getFileAsString(File file) {
        String str = "";
        String line = "";
        BufferedReader reader = null;
        FileInputStream fis = null;
        InputStreamReader isr = null;
        try {
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis , "UTF-8");
            reader = new ExtensionBufferedReader(isr);
            while ((line = reader.readLine()) != null) {
                str += line;
            }
            reader.close();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(JFileUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JFileUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JFileUtil.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(fis);
            IOUtils.closeQuietly(isr);
        }
        return str;
    }

    /**
     * File.mkdirs()がなぜかうまくできないので再帰で
     *
     * @deprecated
     * @param dir
     * @return true: 成功 fattachmentlse: 失敗
     */
    public static boolean mkdirs(File dir) {
        if (dir.exists()) {
            return true;
        }
        File parent = dir.getParentFile();
        if (!parent.exists()) {
            return mkdirs(parent);
        }
        return dir.mkdir();
    }

    /**
     * ファイルおよびディレクトリを再帰的に削除する
     *
     * @param f
     * @return
     */
    private static void removeDirectryRecursive(File f) {
        if (f.exists() == false) {
            return;
        }
        if (f.isFile()) {
            f.delete();
        } else if (f.isDirectory()) {
            /*
             * ディレクトリの場合は、すべてのファイルを削除する
             */
            //  対象ディレクトリ内のファイルおよびディレクトリの一覧を取得
            File[] files = f.listFiles();
            for (File file : files) {
                removeDirectryRecursive(file);
            }
            f.delete();
        }
    }
    
    
    public static final String UTF_8 = "UTF-8";
    public static final String MS932 = "MS932";

    /**
     * MS932 でエンコードされた文字列を UTF-8 に変換する。
     *
     * @param ms932 変換したい文字列
     * @return UTF-8 に変換した文字列
     * @throws UnsupportedEncodingException 変換に失敗した場合
     */
    public static String convertMs932ToUtf8(String ms932) throws UnsupportedEncodingException {
        String temp = new String(ms932.getBytes(MS932), MS932);
        //String temp = new String(ms932.getBytes("iso-8859-1"), "iso-8859-1");
        return new String(temp.getBytes(UTF_8), UTF_8);
    }

    public static String convertUtf8ToMs932(String utf8) throws UnsupportedEncodingException {
        String temp = new String(utf8.getBytes(UTF_8), UTF_8);
        return new String(temp.getBytes(MS932), MS932);
    }

    private static boolean checkCharacterCode(String str, String encoding) {
        if (str == null) {
            return true;
        }

        try {
            byte[] bytes = str.getBytes(encoding);
            return str.equals(new String(bytes, encoding));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("エンコード名称が正しくありません。", ex);
        }
    }

    public static boolean isWindows31j(String str) {
        return checkCharacterCode(str, "MS932");
    }

    public static boolean isSJIS(String str) {
        return checkCharacterCode(str, "SJIS");
    }

    public static boolean isEUC(String str) {
        return checkCharacterCode(str, "euc-jp");
    }

    public static boolean isUTF8(String str) {
        return checkCharacterCode(str, "UTF-8");
    }

    /**
     * 改行コードも取得するBufferedReattachmentder
     */
    private class ExtensionBufferedReader extends BufferedReader {

        public ExtensionBufferedReader(Reader in) {
            super(in);
        }

        public ExtensionBufferedReader(Reader in, int sz) {
            super(in, sz);
        }

        @Override
        public String readLine() throws IOException {
            int num = 0;
            StringBuilder strBfr = new StringBuilder();
            try {
                while ((num = this.read()) >= 0) {
                    strBfr.append((char) num);
                    switch ((char) num) {
                        case '\r':
                        case '\n':
                            return strBfr.toString();
                        default:
                            break;
                    }
                }
            } catch (IOException e) {
                throw e;
            }

            if (strBfr.length() == 0) {
                return null;
            } else {
                return strBfr.toString();
            }
        }
    }
}
