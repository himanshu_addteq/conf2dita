/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.junoe.confluence.plugins;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.gzipfilter.org.apache.commons.lang.ArrayUtils;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.transform.TransformerException;
import jp.junoe.confluence.plugins.conf2dita.ConfluenceSourceDocument;
import jp.junoe.confluence.plugins.conf2dita.dita.TopicDocument;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaTopicType;

import jp.junoe.confluence.plugins.conf2dita.DitaXmlUtil;
import jp.junoe.confluence.plugins.conf2dita.dita.DitaDTD;
import org.apache.commons.collections.ListUtils;

import org.apache.commons.io.FileUtils;
import org.dom4j.Attribute;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.LoggerFactory;

/**
 * ConfluenceとDITAの架け橋をするクラス
 *
 * @author Takashi
 */
public class Conf2dita {

    /**
     * dita
     */
    public static final String DITA = "dita";

    /**
     * ConfluenceのXMLからDITAに変換するXSLのパス
     */
    private static final String conf2dita_xsl_path = "/xslt/conf2dita/conf2dita.xsl";

    /**
     * DITAからConfluenceのXMLに変換するXSLのパス
     */
    private static final String dita2conf_xsl_path = "/xslt/conf2dita/dita2conf.xsl";

    /**
     * DITA <=> Confluenceの変更でimportされるファイルのconfluence_home.temp_dirからのルートディレクトリ
     */
    private static final String xsl_imported_files_path = "/xslt/conf2dita/imported_files";

    /**
     * DITAのTopicIDをContentPropertyに入れておく際のKey名
     */
    public static final String DITA_TOPIC_ID = "jp.junoe.confluence.plugins.conf2dita.dita_topic_id";

    /**
     * DITAのTopicTypeをContentPropertyに入れておく際のKey名
     */
    public static final String DITA_TOPIC_TYPE = "jp.junoe.confluence.plugins.conf2dita.dita_topic_type";

    /**
     * ページについているラベルを頼りにDITAのTopicTypeを決める際、その目印になるPrefix
     */
    public static final String DITA_TOPIC_TYPE_LABEL_PREFIX = "dita-";

    /**
     * ページについているラベルを頼りにそのDITAがインポートされた際のTopicTypeを示すPrefix
     */
    public static final String DITA_TOPIC_TYPE_LABEL_IMPORTED_PREFIX = "ditaimported-";

    /**
     * DitaのプロパティをXMLやHTMLにするときに属性として付与する際のPrefix.<br >
     * used also in JavaScript editor.js and XSL dita2confImpl.xsl
     */
    public static final String DITA_PROPERTY_PREFIX = "ditaProperty";

    /**
     * 他のシステムで作成したDITAXMLをインポートした際にそのDITAのTOPIC_IDがアサインされるContentPropertyのキー名
     *
     * @todo not implemented yet
     */
    // public static final String PROPERTY_NAME_DITA_IMPORTED_ID = "jp.junoe.confluence.plugins.conf2dita.dita_imported_original_id";
    /**
     * DITAに出力した際にmetadata/othermetaの中に含めるページIDの名前属性の値
     */
    public static final String CONF2DITA_PAGEID_ATTRIBUTE = "conf2dita_pageid";

    /**
     * DITAに出力した際にmetadata/othermetaの中に含めるスペースキーの名前属性の値
     */
    public static final String CONF2DITA_SPACEKEY_ATTRIBUTE = "conf2dita_spacekey";

    /**
     * DITAに出力した際にIDを入れる際、特に指定が無かった場合はsmartdita-1234(page_id)のようにする
     */
    public static final String CONF2DITA_DITAID_AUTO_GENERATED_PREFIX = "conf2dita-";

    /**
     * このConfluenceページに紐づけられているDITAのTOPICTYPEを返す
     *
     * @param page
     * @param cm
     * @return
     */
    public static String getTopicType(ContentEntityObject page, ContentPropertyManager cm) {
        String topicType;
        DitaTopicType ditaTopicType = DitaTopicType.load(page, cm);
        topicType = ditaTopicType.getTopicType();
        if (topicType == null) {
            topicType = "";
        }
        return topicType;
    }

    /**
     * このConfluenceページに紐づけられたDITAのTopicIDを返す
     *
     * @param page
     * @param contentPropertyManager
     * @return
     */
    public static String getTopicId(ContentEntityObject page, ContentPropertyManager contentPropertyManager) {
        String ditaId = contentPropertyManager.getStringProperty(page, Conf2dita.DITA_TOPIC_ID);
        if (StringUtils.isEmpty(ditaId)) {
            // ditaId = contentPropertyManager.getStringProperty(page, Conf2dita.PROPERTY_NAME_DITA_IMPORTED_ID);
        }
        if (StringUtils.isEmpty(ditaId)) {
            ditaId = CONF2DITA_DITAID_AUTO_GENERATED_PREFIX + page.getIdAsString();
        }
        if (StringUtils.isEmpty(ditaId)) {
            ditaId = "";
        }
        return ditaId;
    }

    /**
     * Pageの親の方まで遡ってTopicIDを取得していく
     *
     * @param page　対象とするページ
     * @param cm
     * @param rootPage rootとみなすページ。nullの場合は、スペースのrootまで取りに行く
     * @return
     */
    public static String[] getAncestorTopicIds(Page page, ContentPropertyManager cm, Page rootPage) {
        List<String> topicIds = new ArrayList<String>();
        long rootId = 0;
        if (rootPage != null) {
            rootId = rootPage.getId();
        }

        Page parentPage = page.getParent();
        while (parentPage != null) {
            topicIds.add(getTopicId(parentPage, cm));
            if (parentPage.getId() == rootId) {
                break;
            } else {
                parentPage = parentPage.getParent();
            }
        }
        String[] toArray = topicIds.toArray(new String[0]);
        ArrayUtils.reverse(toArray);
        return toArray;
    }

    /**
     * ConfluenceのページからDitaXMLのDocumentを返す
     *
     * @todo
     * @see Conf2dita#c2d(com.atlassian.confluence.pages.Page,
     * java.lang.String,java.lang.String)
     * @param contentPropertyManager
     * @param page
     * @param topicType
     * @return
     * @throws TransformerException
     * @throws Exception
     */
    public static TopicDocument c2d(ContentPropertyManager contentPropertyManager, Page page, String topicType) throws TransformerException, Exception {
        String ditaId = getTopicId(page, contentPropertyManager);
        return _c2d(page, topicType, ditaId, contentPropertyManager);
    }

    /**
     * ConfluenceのページからDitaXMLのDocumentを返す（DITAのTOPIC IDを指定済み）
     *
     * @param page
     * @param topicType
     * @param ditaId DitaXMLにアサインするべきID 他システムからインポートした際はそのシステムのIDを保管しておき再登板
     * @param cm
     * @return
     * @throws javax.xml.transform.TransformerException
     * @throws Exception *
     */
    private static TopicDocument _c2d(Page page, String topicType, String ditaId, ContentPropertyManager cm) throws TransformerException, Exception {
        ConfluenceSourceDocument confdoc = null;
        TopicDocument dita = null;
        String confXml = null;
        String ditaXml = null;
        String dtdDir = null;
        String sourceXSL = "";
        setImportedXSLTFiles();

        try {
            dtdDir = DitaDTD.setDTDFiles();
            confdoc = new ConfluenceSourceDocument(page, topicType, ditaId, cm);
            confXml = confdoc.asXML();
//            LoggerFactory.getLogger(Conf2dita.class).error(confXml);
            if (topicType.equals("glossary")) {
                throw new Exception("glossary topic type is not supported yet.");
            } else {
                dtdDir += "/" + DitaXmlUtil.OASIS_DOMAIN + "/dita/v1.2/os/dtd1.2/technicalContent/dtd";
            }
            sourceXSL = getXSLTSource(conf2dita_xsl_path);
            ditaXml = JConfluenceXSLTUtil.parseXSLT(sourceXSL, confXml, dtdDir);
            // LoggerFactory.getLogger(Conf2dita.class).error(ditaXml);
            dita = new TopicDocument(ditaXml);
        } catch (TransformerException ex) {
            throw ex;
        } catch (DocumentException ex) {
            throw (Exception) ex;
        } catch (IOException ex) {
            throw (Exception) ex;
        }

        dita = postfixC2d(dita, page);

        return dita;
    }

    /**
     * DITAXMLからConfluenceのXMLを返す
     *
     * @param ditafile
     * @return
     * @throws Exception
     */
    public static ConfluenceSourceDocument d2c(File ditafile) throws Exception {
        ConfluenceSourceDocument confDoc;
        String confXml = "";
        String ditaXml = "";
        String dtd_path = null;
        String sourceXSL = "";
        setImportedXSLTFiles();

        try {
            dtd_path = new File(JConfluenceXSLTUtil.setConfluenceDTDFiles()).getParent();
            ditaXml = FileUtils.readFileToString(ditafile, "UTF-8");
            ditaXml = DitaDTD.replaceToLocalDTD(ditaXml);
            sourceXSL = getXSLTSource(dita2conf_xsl_path);
            confXml = JConfluenceXSLTUtil.parseXSLT(sourceXSL, ditaXml, dtd_path);
            confDoc = new ConfluenceSourceDocument(confXml);
        } catch (DocumentException ex) {
            throw (Exception) ex;
        } catch (IOException ex) {
            throw (Exception) ex;
        } catch (Exception ex) {
            throw ex;
        }

        return confDoc;
    }

    /**
     * XSLTのソースを取得する
     *
     * @param xsltの位置 dita2conf_xsl_path とかの相対パス
     * @return
     */
    private static String getXSLTSource(String xslt_path) {
        Map<String, Object> xslMap = new HashMap<String, Object>();
        String xsl_import_root_path = "";
        String xslt_path_absolute = JConfluenceFileUtil.getTemporaryDir() + xsl_imported_files_path;
        /*
         // XSLT自体はConfluenceのtempディレクトリに入れたけど、XSLTのimportはcatalina.baseから読みだそうとするので
         // 開発環境では、catalina.baseがXSLT::importのRootだったのでbaseを取得するが、BASEはOptionalなのでHOMEもカバーしておく => 面倒なので絶対パスで指定した
         String catalina_base = System.getProperty("catalina.base");
         if (StringUtils.isEmpty(catalina_base)) {
         catalina_base = System.getProperty("catalina.home");
         }
         xsl_import_root_path = JConfluenceFileUtil.getRelativeDiffPath(catalina_base, xslt_path_absolute);
         */
        xsl_import_root_path = xslt_path_absolute;
        xsl_import_root_path = StringUtils.replace(xsl_import_root_path, "\\", "/");
        xslMap.put("xsl_import_root_path", xsl_import_root_path);

        String sourceXSL = JConfluenceXSLTUtil.getXSLTSource(xslt_path, xslMap);
        return sourceXSL;
    }

    /**
     * ConfluenceからDITAに変換したばかりのファイルを調整する
     *
     * @param dita
     * @param page
     * @return
     * @throws java.lang.Exception
     */
    public static TopicDocument postfixC2d(TopicDocument dita, Page page) throws Exception {
        String topicType = dita.topicType;
        /// スペースキーとページIDの埋め込み
        // いったん削除
        List<Element> othermeta = dita.selectNodes("//prolog/metadata/othermeta");
        int size = othermeta.size();
        for (int i = size - 1; i >= 0; i--) {
            Attribute attr_n = othermeta.get(i).attribute("name");
            if (attr_n != null) {
                if (attr_n.getValue().equals(CONF2DITA_PAGEID_ATTRIBUTE) || attr_n.getValue().equals(CONF2DITA_SPACEKEY_ATTRIBUTE)) {
                    othermeta.get(i).getParent().remove(othermeta.get(i));
                }
            }
        }
        // 埋め込み
        Element prolog = null;
        List prologs = dita.selectNodes("//prolog");
        if (prologs.isEmpty()) {
            prolog = DocumentHelper.createElement("prolog");
            // prologはbodyの前
            List content = dita.getRootElement().content();
            String bodytag = DitaXmlUtil.getTopicBodyQName(topicType);
            boolean setProlog = false;
            for (Object c1 : content) {
                if (c1 instanceof Element) {
                    Element c = (Element) c1;
                    if (c.getQualifiedName().toLowerCase().equals(bodytag)) {
                        int index = content.indexOf(c1);
                        content.add(index, prolog);
                        setProlog = true;
                        break; // if not break, infinite loop occures.
                    }
                }
            }
            if (!setProlog) {
                throw new Exception("Prolog add Error. bodytag:" + bodytag + "  roottag:" + topicType);
            }
        } else {
            prolog = (Element) prologs.get(0);
        }
        Element metadata = null;
        List metadatas = prolog.selectNodes("metadata");
        if (metadatas.isEmpty()) {
            metadata = prolog.addElement("metadata");
        } else {
            metadata = (Element) metadatas.get(0);
        }
        Element othermeta_pageid = metadata.addElement("othermeta");
        othermeta_pageid.add(DocumentHelper.createAttribute(othermeta_pageid, "name", CONF2DITA_PAGEID_ATTRIBUTE));
        othermeta_pageid.add(DocumentHelper.createAttribute(othermeta_pageid, "content", page.getIdAsString()));
        Element othermeta_spacekey = metadata.addElement("othermeta");
        othermeta_spacekey.add(DocumentHelper.createAttribute(othermeta_spacekey, "name", CONF2DITA_SPACEKEY_ATTRIBUTE));
        othermeta_spacekey.add(DocumentHelper.createAttribute(othermeta_spacekey, "content", page.getSpaceKey()));

        // rootからxmlnsとditaarch以外削除
        Element root = dita.getRootElement();
        List<Attribute> rootAttr = root.attributes();
        size = rootAttr.size();
        for (int i = size - 1; i >= 0; i--) {
            Attribute a = rootAttr.get(i);
            String attrName = a.getQualifiedName();
            if (!attrName.toLowerCase().equals("id")) {
                Attribute a2 = root.attribute(a.getQName());
                if (a2 != null) {
                    root.remove(a2);
                }
            }
        }

        // class属性を削除
        List selectNodes = dita.getRootElement().selectNodes("//*");
        Element n = null;
        Attribute a = null;
        for (Object node : selectNodes) {
            if (node instanceof Element) {
                n = (Element) node;
                a = n.attribute("class");
                if (a != null) {
                    n.remove(a);
                }
            }
        }

        return dita;
    }

    /**
     * importで使用するファイルをConfluenceのTemporary領域にコピーする
     *
     * @return XSLT内でimportで使用するファイルのルートパスを返す
     */
    private static String setImportedXSLTFiles() {
        ArrayList<String> files = new ArrayList<String>();
        files.addAll(Arrays.asList(xslfile_dita_ot_18M2));
        String rootDir = JConfluenceFileUtil.getTemporaryDir() + xsl_imported_files_path;
        String xslContent;
        Map dummyMap = null;

        String xslFileDest, xslSourceFile;
        File xslFileDestFile, xslFileDestDir;

        for (String filepath : files) {
            xslFileDest = rootDir + "/" + filepath;
            xslFileDestFile = new File(xslFileDest);
            if (!xslFileDestFile.exists()) {
                xslFileDestDir = xslFileDestFile.getParentFile();
                if (xslFileDestDir.exists() || xslFileDestDir.mkdirs()) {
                    xslSourceFile = xsl_imported_files_path + "/" + filepath;
                    xslContent = VelocityUtils.getRenderedTemplate(xslSourceFile, dummyMap);
                    try {
                        FileUtils.writeStringToFile(xslFileDestFile, xslContent, "UTF-8");
                    } catch (IOException ex) {
                    }
                }
            }
        }

        return rootDir;
    }

    /**
     * version 1.2
     */
    private static final String[] xslfile_dita_ot_18M2 = {
        "conf2ditaFunctions.xsl",
        "conf2ditaImpl.xsl",
        "dita2confImpl.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/dita2html-base.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/dita2html-base_template.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/dita2html.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/dita2html5.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/dita2xhtml.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2html5toc.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2htmltoc.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2htmtoc/map2htmlImpl.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2htmtoc/map2htmtocImpl.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2htmtoc.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2htmtoc_template.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/map2xhtmtoc.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/abbrev-d.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/conceptdisplay.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/dita2html5Impl.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/dita2htmlImpl.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/flag-old.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/flag.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/get-meta.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/glossdisplay.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/hi-d.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/hi-d2html5.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/htmlflag.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/map2TOC.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/mapwalker.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/pr-d.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/refdisplay.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/rel-links.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/sw-d.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/syntax-braces.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/syntax-svg.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/taskdisplay.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/ui-d.xsl",
        "DITA-OT/1.8.M2/plugins/org.dita.xhtml/xsl/xslhtml/ut-d.xsl",
        "DITA-OT/1.8.M2/xsl/common/allstrings.xml",
        "DITA-OT/1.8.M2/xsl/common/allstrings_template.xml",
        "DITA-OT/1.8.M2/xsl/common/dita-textonly.xsl",
        "DITA-OT/1.8.M2/xsl/common/dita-utilities.xsl",
        "DITA-OT/1.8.M2/xsl/common/flag.xsl",
        "DITA-OT/1.8.M2/xsl/common/map2textonly.xsl",
        "DITA-OT/1.8.M2/xsl/common/output-message.xsl",
        "DITA-OT/1.8.M2/xsl/common/related-links.xsl",
        "DITA-OT/1.8.M2/xsl/common/strings-ar-eg.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-be-by.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-bg-bg.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ca-es.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-cs-cz.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-da-dk.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-de-ch.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-de-de.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-el-gr.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-en-ca.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-en-gb.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-en-us.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-es-es.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-et-ee.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-fi-fi.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-fr-be.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-fr-ca.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-fr-ch.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-fr-fr.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-he-il.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-hi-in.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-hr-hr.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-hu-hu.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-id-id.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-is-is.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-it-ch.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-it-it.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ja-jp.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-kk-kz.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ko-kr.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-lt-lt.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-lv-lv.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-mk-mk.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ms-my.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-nl-be.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-nl-nl.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-no-no.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-pl-pl.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-pt-br.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-pt-pt.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ro-ro.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ru-ru.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-sk-sk.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-sl-si.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-sr-latn-rs.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-sr-sp.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-sv-se.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-th-th.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-tr-tr.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-uk-ua.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-ur-pk.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-zh-cn.xml",
        "DITA-OT/1.8.M2/xsl/common/strings-zh-tw.xml",
        "DITA-OT/1.8.M2/xsl/common/strings.xml",
        "DITA-OT/1.8.M2/xsl/common/topic2textonly.xsl",
        "DITA-OT/1.8.M2/xsl/common/ui-d2textonly.xsl",
        "DITA-OT/1.8.M2/xsl/generalize.xsl",
        "DITA-OT/1.8.M2/xsl/job-helper.xsl",
        "DITA-OT/1.8.M2/xsl/normalize.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/conref.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/conrefImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/conref_template.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/flag.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/flagImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/flag_template.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/maplink.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/maplinkImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/maplink_template.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/mappull.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/mappullImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/mappull_template.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/mapref.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/maprefImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/mapref_template.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/topicpull-pr-d.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/topicpull-task.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/topicpull.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/topicpullImpl.xsl",
        "DITA-OT/1.8.M2/xsl/preprocess/topicpull_template.xsl",
        "DITA-OT/1.8.M2/xsl/pretty.xsl",
        "DITA-OT/1.8.M2/xsl/specialize.xsl"
    };

    /**
     * Dita拡張子かチェック
     *
     * @param file
     * @return
     */
    public static boolean isDitaExt(File file) {
        String name = file.getName();
        return name.toLowerCase().endsWith(DitaXmlUtil.DITA_FILE_EXTENSION);
    }

    /**
     * base64 encoding/decoding class instead of ("=", "_") and ("+", "@")
     */
    public static class Base64 {

        private static final String asEqual = "_";

        private static final String asPlus = "@";

        /**
         * base64 decoded String to Map
         *
         * @param queryParams
         * @return
         */
        public static String encodeQueryParams(Map<String, String> queryParams, String asQPAmp, String asQPEqual) {

            List<String> queryParamsList = new ArrayList<String>();
            String queryParamsString = "";
            byte[] queryParamsBytes;
            String decoded = "";

            for (Map.Entry<String, String> element : queryParams.entrySet()) {
                queryParamsList.add(element.getKey() + asQPEqual + element.getValue());
            }
            queryParamsString = StringUtils.join(queryParamsList, asQPAmp);

            try {
                queryParamsBytes = queryParamsString.getBytes("UTF-8");
                queryParamsBytes = org.apache.commons.codec.binary.Base64.encodeBase64(queryParamsBytes);
                queryParamsString = new String(queryParamsBytes);
            } catch (UnsupportedEncodingException ex) {
            }
            decoded = replaceEqualPlus(queryParamsString);

            return decoded;
        }

        /**
         * Map queryparams to base64 decoded String
         *
         * @param queryParamsString
         * @param asQPAmp
         * @param asQPEqual
         * @return
         */
        public static Map<String, List<String>> decodeQueryParams(String queryParamsString, String asQPAmp, String asQPEqual) {
            byte[] queryParamsBytes;

            Map<String, List<String>> queryParams = new HashMap<String, List<String>>();

            queryParamsString = replaceEqualPlus(queryParamsString);

            try {
                queryParamsBytes = queryParamsString.getBytes("UTF-8");
                queryParamsBytes = org.apache.commons.codec.binary.Base64.decodeBase64(queryParamsBytes);
                queryParamsString = new String(queryParamsBytes);
            } catch (UnsupportedEncodingException ex) {

            }


            try {
                queryParams = JConfluenceHTTPUtil.parseQuery(queryParamsString, asQPAmp, asQPEqual);
            } catch (UnsupportedEncodingException ex) {
            }

            return queryParams;
        }

        private static String replaceEqualPlus(String str) {
            str = StringUtils.replace(str, Conf2dita.Base64.asEqual, "=");
            str = StringUtils.replace(str, Conf2dita.Base64.asPlus, "+");
            return str;
        }

    }
}
